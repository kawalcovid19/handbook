# Nilai-Nilai Tim Teknologi Komunitas KawalCOVID19

## Tentang Nilai-Nilai Ini

Nilai-nilai kita sebagai tim teknologi komunitas KawalCOVID19 antara lain yakni [🤝 Kolaborasi](#kolaborasi), [📈 Hasil](#hasil), [⏱️ Efisiensi](#efisiensi), [🌐 Keberagaman](#keberagaman), [👣 Iterasi](#iterasi), dan [👀 Transparansi](#transparansi).

Nilai-nilai tersebut diadopsi dari [nilai-nilai perusahaan GitLab](https://about.gitlab.com/handbook/values) karena KawalCOVID19 memiliki banyak kemiripan dengan perusahaan ini, antara lain:

- GitLab adalah perusahaan [*all-remote*](https://about.gitlab.com/company/culture/all-remote/) terbesar di dunia, sebagaimana KawalCOVID19 yang bekerja dengan sistem *remote* sepenuhnya tanpa tatap muka sama sekali.
- Ukuran perusahaan GitLab sangat besar, [lebih dari 1200 orang anggota tim](https://about.gitlab.com/company/team/), sebagaimana KawalCOVID19 yang memiliki lebih dari 800 orang relawan terdaftar.
- Anggota tim GitLab tersebar di [68 negara](https://about.gitlab.com/company/team/#countries) dengan zona waktu yang sangat beragam, sebagaimana anggota tim KawalCOVID19 yang tersebar di setidaknya 3 zona waktu di Indonesia dan 11 zona waktu lainnya di manca negara.
- [Cakupan produk GitLab yang sangat luas](https://about.gitlab.com/devops-tools/) untuk ukuran perusahaan yang baru diluncurkan di tahun 2011, lebih muda beberapa tahun dari perusahaan-perusahaan pesaingnya. KawalCOVID19 pun ingin bisa meniru kecepatannya dalam mengembangkan produk, mengingat diperlukannya kesigapan yang tinggi untuk penanganan COVID-19 di Indonesia.

Siapapun berhak untuk mengajukan perbaikan atau perubahan terhadap nilai-nilai ini dengan cara mengajukan *Merge Request* melalui tombol **Edit halaman ini** di bagian bawah halaman ini.

## Kolaborasi

> Adopsi atas [🤝 *Collaboration*](https://about.gitlab.com/handbook/values/#collaboration)

Prioritaskan untuk membantu orang lain, bahkan meskipun tidak secara langsung berhubungan dengan tujuan yang sedang ingin Anda capai di tim. Di lain sisi, Anda juga bisa (dan sangat dianjurkan untuk) meminta bantuan dan saran dari orang lain untuk memperlancar pencapaian tujuan Anda di tim.

Siapapun boleh nimbrung dan memberikan saran di topik pembahasan manapun, termasuk orang-orang yang belum terdaftar sebagai relawan KawalCOVID19. Namun demikian, pengambilan keputusan tetap ada di tangan penanggung jawab (*Person In Charge*), dengan tetap mempertimbangkan setiap masukan secara sungguh-sungguh serta berusaha untuk menanggapi dan menjelaskan mengapa masukan-masukan tersebut diterapkan atau tidak.

### Berprasangka baik

> Adopsi atas [*Assume positive intent*](https://about.gitlab.com/handbook/values/#assume-positive-intent)

Kita pada umumnya memiliki standard ganda ketika menilai tindakan orang lain. Kita menyalahkan lingkungan untuk kesalahan kita sendiri, tapi kita menyalahkan individu untuk kesalahan orang lain. Standard ganda ini disebut juga dengan [*Fundamental Attribution Error*](https://en.wikipedia.org/wiki/Fundamental_attribution_error). Dalam rangka mitigasi terhadap perilaku ini, kita harus selalu berupaya untuk [berprasangka baik](https://www.collaborativeway.com/general/a-ceos-advice-assume-positive-intent/) dalam berinteraksi dengan orang lain, menghormati keahlian mereka, dan berusaha menyenangkan apabila terjadi kesalahan.

Ketika [tidak setuju dengan orang lain](#berkomitmen-dalam-ketidaksetujuan), terkadang kita mendebat poin-poin terlemah dari argumen lawan, atau bahkan mendebat si "manusia jerami (*straw man*)". Asumsikan poin-poin argumen lawan tersebut diajukan dengan niat baik dan berusahalah untuk [mendebat sang "manusia baja (*steel man*)"](https://desert.glass/newsletter/week-46/).

> Ini terjadi ketika kita mengungkapkan versi terkuat mutlak dari argumen lawan kita yang bahkan bisa jadi lebih kuat daripada argumen yang dikemukakan oleh lawan. Argumen "manusa baja" yang baik adalah argumen di mana lawan kita merasa bahwa kita sudah mengemukakan kembali argumen mereka dengan baik, bahkan meskipun mereka masih tidak setuju dengan asumsi atau kesimpulan kita.

### Penyelesaian masalah tanpa menyalahkan

> Adopsi atas [*Blameless problem solving*](https://about.gitlab.com/handbook/values/#blameless-problem-solving)

...

### Tidak mungkin untuk mengetahui segalanya

> Adopsi atas [*It's impossible to know everything*](https://about.gitlab.com/handbook/values/#its-impossible-to-know-everything)

...

## Hasil

> Adopsi atas [📈 *Results*](https://about.gitlab.com/handbook/values/#results)

...

### Kepemilikan

> Adopsi atas [*Ownership*](https://about.gitlab.com/handbook/values/#ownership)

...

### Kesadaran terhadap urgensi

> Adopsi atas [*Sense of urgency*](https://about.gitlab.com/handbook/values/#sense-of-urgency)

...

### Menerima ketidakpastian

> Adopsi atas [*Accepting uncertainty*](https://about.gitlab.com/handbook/values/#accepting-uncertainty)

...

## Efisiensi

> Adopsi atas [⏱️ *Efficiency*](https://about.gitlab.com/handbook/values/#efficiency)

...

### Tuliskan segala sesuatu

> Adopsi atas [*Write things down*](https://about.gitlab.com/handbook/values/#write-things-down)

...

### Mandiri dalam bekerja dan belajar

> Adopsi atas [*Self-service and self-learning*](https://about.gitlab.com/handbook/values/#self-service-and-self-learning)

...

### Solusi yang membosankan

> Adopsi atas [*Boring solutions*](https://about.gitlab.com/handbook/values/#boring-solutions)

...

## Keberagaman

> Adopsi atas [🌐 *Diversity & Inclusion*](https://about.gitlab.com/handbook/values/#diversity-inclusion)

...

### Kecenderungan terhadap komunikasi asinkron

> Adopsi atas [*Bias towards asynchronous communication*](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication)

...

### Membangun komunitas yang aman

> Adopsi atas [*Building a safe community*](https://about.gitlab.com/handbook/values/#building-a-safe-community)

...

### Keluarga dan teman dahulu, pekerjaan kemudian

> Adopsi atas [*Family and friends first, work second*](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second)

...

## Iterasi

> Adopsi atas [👣 *Iteration*](https://about.gitlab.com/handbook/values/#iteration)

...

### Perubahan sekecil mungkin

> Adopsi atas [*Minimum Viable Change (MVC)*](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc)

...

### Rasa malu yang rendah

> Adopsi atas [*Low level of shame*](https://about.gitlab.com/handbook/values/#low-level-of-shame)

...

## Transparansi

> Adopsi atas [👀 *Transparency*](https://about.gitlab.com/handbook/values/#transparency)

...

### Serba publik

> Adopsi atas [*Public by default*](https://about.gitlab.com/handbook/values/#public-by-default)

...

### Sumber tunggal kebenaran

> Adopsi atas [*Single Source of Truth*](https://about.gitlab.com/handbook/values/#single-source-of-truth)

...

### Berkomitmen dalam ketidaksetujuan

> Adopsi atas [*Disagree, commit, and disagree*](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree)

...
