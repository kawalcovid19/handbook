module.exports = [
  {
    text: 'Overview',
    link: '/overview',
  },
  {
    text: 'Values',
    link: '/values'
  },
  {
    text: 'Architecture',
    link: '/architecture',
  },
  {
    text: 'Workflow Content',
    items: [
      {
        text: 'General Overview',
        link: '/workflow-content/0-general-overview',
      },
      {
        text: 'Writing Content',
        link: '/workflow-content/1-writing-content',
      },
      {
        text: 'Approving Content',
        link: '/workflow-content/2-approving-content',
      },
      {
        text: 'Releasing Content',
        link: '/workflow-content/3-releasing-content',
      },
    ],
  },
  {
    text: 'GIS',
    link: '/gis',
  },
  {
    text: 'Wishlist',
    link: '/wishlist',
  },
  {
    text: 'Meeting',
    link: '/meeting',
  },
  {
    text: 'Glosarium',
    link: '/glosarium',
  },
]
