---
prev: false
next: /workflow-content/1-writing-content
---

# General Overview Workflow Content

List team:

* #team-content - Administrator | [Guideline](./3.%20Releasing%20Content.md)

## Current Workflow

```mermaid
sequenceDiagram
	Note over Author,Editor: draft
	Author->>+Editor: Submit Draft
	Note over Author,Editor: in_review
	Editor-->>+Author: Request Changes
	Author-->>+Editor: Submit Changes
	Editor->>+Administrator: Approve
	Note over Editor,Administrator: approved
	Administrator->>+Administrator: Reformat & Publish
	Note over Administrator: published
	Administrator-->>+Website: Netlify Publish
	Note over Administrator, Website: published & displayed
```

## Document State

```mermaid
stateDiagram
	[*] --> draft
	draft --> in_review
	in_review --> draft
	in_review --> approved
	approved --> in_review
	approved --> publish
	publish --> displayed: Netlify Publish
	displayed -->	[*]
```
