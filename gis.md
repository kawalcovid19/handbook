# Geographical Information System

## Data GIS yg tersedia

- Batas daerah dari Dukcapil Kemendagri
- Data demografi dari Dukcapil Kemendagri
- RS Rujukan Corona dari Kemenkes
- Berita online (news crawling) dari Geospark Analytics
- Travel advisory dari Department of State (semacam Kemenlu nya USA)

## Aplikasi yg akan dibuat

> to note: usahakan semua aplikasi mobile-friendly

- [x] Aplikasi Peta RS Rujukan Corona
- [x] Aplikasi Dashboard News Explorer:
  - Website [bit.ly/kawalcovid](https://bit.ly/kawalcovid) (desktop & mobile friendly)
  - [API Source](https://services6.arcgis.com/SQuJmDxiYz3gToFu/arcgis/rest/services/Geospark_Analytics_Hyperion_Events_Prod/FeatureServer/0/query?f=json&where=title%20LIKE%20%27%25corona%25%27&returnGeometry=false&spatialRel=esriSpatialRelIntersects&geometry=%7B%22xmin%22%3A-9331126.719294008%2C%22ymin%22%3A-3692990.5433928035%2C%22xmax%22%3A9062679.767246034%2C%22ymax%22%3A10356746.75164523%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D&geometryType=esriGeometryEnvelope&inSR=102100&outFields=*&outSR=102100&resultOffset=0&resultRecordCount=1) by Geospark
- [x] Aplikasi Travel advisory - data dari Dephub US
- [x] Open Data - on process
- [x] Coronavirus Indonesia Case Map seperti [www.coromap.info](https://www.coromap.info/) (kalau data valid nya bisa diperoleh)

## Open Data

> to note: saat ini kita menggunakan [ArcGIS Online](https://arcgis.com/), sebuah cloud-based SaaS untuk solusi pemetaan dan analisis spasial. Tetapi ArcGIS Online ini hibah dari Esri inc. Karena bersifat hibah, masa berlakunya hanya 3 bln. Selanjutnya akan di host di Esri Indonesia.**
> last update: menyiapkan environment di esri indonesia

- [x] Menyelesaikan landing page
- [ ] Tutorial penggunaan Open Data (video / teks)
- [x] Migrasi konten ke environment arcgis online milik esri indonesia --> URL harus diganti
- [ ] Custom URL dgn domain [kawalcovid19.id](https://kawalcovid19.id/)
- [x] Google Analytics (nice to have)
