# Ketersediaan Volunteer

Menampilkan volunteer yang masih belum memiliki tugas

| Nama | Bidang Keahlian | Status |
| ----------- | ----------- | ----------- |
| Abdi Wibowo Maarif | UI/UX design | Available |
| Agit Naeta | Pemetaan Web (Maps) | Available |
| Arnold Saputra | UX design & research | Available |
| Dida | BackEnd (Ruby & JS) dan dokumentasi | Available |
| Didi Kurniawan | UI/UX Designer | Available |
| dr. Dike Hanurafinova Afifi | Edukasi Medis | Available |
|Kanya Paramita | Data | Available|
| Khairul Anshar | backend+frontend | Available |
| Massam | Front End | Available |
| Rayrizaldy | Mobile (tapi udah ga coding), UX, Product. | Available |
| Rio Bahtiar | WordPress/Laravel dan Golang | Available |
| Rizky Maulana Nugraha | GIS, Backend (Python, JS), Docker | Available |
| Septia Agustin | | Available |
| Sonny Prakoso | UX Design x Frontend | Available |
| Thaariqh alfath | UI/UX Designer | Available |
| Novita | User Research | Available |
| Argawi Kandito | Virology | Available |
| Angga Fauzan | UIUX Design, Campaign, Content, Digital Marketing | Available |
| Frankybpharm |  input untuk clinical data dan clinical process | Available |
| M Faiz Ghifari | Butuh Driver App | Available |
| Reza Radityo | Product Designer | Available |
| Danang Arbansa | Frontend Engineer | Available |
| Roby Huzwandar | frontend engineer : react-native/flutter/reactjs | Available |
| Muhammad Edwin | backend + devops + gis | Available |
| Hadi Syah Putra | data analysis | Available |
